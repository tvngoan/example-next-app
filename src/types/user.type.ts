export interface User {
  id: number;
  role: 'admin' | 'user';
}
