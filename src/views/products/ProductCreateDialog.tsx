import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  FormHelperText,
  LinearProgress,
  TextField,
} from '@mui/material';
import React, { useEffect } from 'react';
import { Product } from '../../types/product.type';
import useProductCreate from '../../hooks/product/useProductCreate';
// form
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

type Props = {
  open: boolean;
  onClose: () => void;
  onCreated: (data: Product) => void;
};

const schema = yup.object().shape({
  name: yup.string().min(5).max(40).required(),
  price: yup.number().min(1).max(1000).required(),
});

const defaultValues = {
  name: '',
  price: 0,
};

function ProductCreateDialog({ open, onClose, onCreated }: Props) {
  const [{ loading, error }, doCreate] = useProductCreate({
    // mock data
    name: '',
    price: 0,
    thumbnail: 'https://loremflickr.com/500/500/animals',
    id: 99999 * Math.random(),
  });
  const {
    reset,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues,
    mode: 'onChange',
    resolver: yupResolver(schema),
  });

  const onSubmit = (data: Partial<Product>) => {
    doCreate({ data }).then(res => {
      if (res.status == 201) {
        onClose();
        onCreated({ ...res.data, ...data }); // in real-project, it should be onCreated(res.data);
        reset();
      }
    });
  };

  useEffect(() => {
    reset();
  }, [open, reset]);

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth='sm'>
      <DialogTitle>Create Product</DialogTitle>
      {loading && <LinearProgress />}
      <DialogContent>
        <Box id='product-create-form' component={'form'} onSubmit={handleSubmit(onSubmit)} mt={2}>
          <FormControl fullWidth sx={{ mb: 3 }}>
            <Controller
              name='name'
              control={control}
              rules={{ required: true }}
              render={({ field: { value, onChange } }) => (
                <TextField
                  value={value}
                  label='Name'
                  onChange={onChange}
                  placeholder='Enter a name'
                  error={Boolean(errors.name)}
                />
              )}
            />
            {errors.name && <FormHelperText sx={{ color: 'error.main' }}>{errors.name.message}</FormHelperText>}
          </FormControl>
          <FormControl fullWidth sx={{ mb: 3 }}>
            <Controller
              name='price'
              control={control}
              rules={{ required: true }}
              render={({ field: { value, onChange } }) => (
                <TextField
                  value={value}
                  label='Price'
                  onChange={onChange}
                  placeholder='Enter a price'
                  error={Boolean(errors.price)}
                  type='number'
                />
              )}
            />
            {errors.price && <FormHelperText sx={{ color: 'error.main' }}>{errors.price.message}</FormHelperText>}
          </FormControl>
        </Box>
        {error && <DialogContentText color={'error'}>Error: {error.message}</DialogContentText>}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} disabled={loading}>
          Close
        </Button>
        <Button type='submit' form='product-create-form' color='success' disabled={loading}>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ProductCreateDialog;
