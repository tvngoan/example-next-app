import { Avatar, IconButton, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import React from 'react';

function UserMenu() {
  const router = useRouter();
  const { t } = useTranslation();
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

  const usrItems = [
    { name: t('Profile'), path: '/profile' },
    { name: t('Account'), path: '/account' },
    { name: t('Logout'), path: '/logout' },
  ];

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleUsrItemClick = (path: string) => () => {
    handleCloseUserMenu();
    router.push(path);
  };

  return (
    <>
      <Tooltip title={t('Open settings')}>
        <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
          <Avatar alt='Remy Sharp' src='/static/images/avatar/2.jpg' />
        </IconButton>
      </Tooltip>
      <Menu
        sx={{ mt: '45px' }}
        id='menu-appbar'
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorElUser)}
        onClose={handleCloseUserMenu}
      >
        {usrItems.map(({ name, path }) => (
          <MenuItem key={path} onClick={handleUsrItemClick(path)}>
            <Typography textAlign='center'>{name}</Typography>
          </MenuItem>
        ))}
      </Menu>
    </>
  );
}

export default UserMenu;
