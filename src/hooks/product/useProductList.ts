import { mockProductLists } from '../../mocks/product.mock';
import { Product, ProductGetParams } from '../../types/product.type';
import useAxios from '../shared/useAxiosWrapper';

/**
 * Topic: Manage Products
 *
 * Feature: Get product list
 *
 * @returns
 */
function useProductList(params: ProductGetParams) {
  const { offset = 0, limit = 10, keyword = '' } = params ?? {};
  return useAxios<Product[]>(
    {
      method: 'GET',
      url: '/products',
      params,
    },
    {
      mockData: mockProductLists
        .filter(product => product.name.toLowerCase().includes(keyword.toLowerCase()))
        .slice(offset, offset + limit),
    },
  );
}

export default useProductList;
