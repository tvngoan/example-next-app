export interface Product {
  id: number;
  thumbnail: string;
  name: string;
  price: number;
}

export interface ProductGetParams {
  keyword?: string;
  limit?: number;
  offset?: number;
}
