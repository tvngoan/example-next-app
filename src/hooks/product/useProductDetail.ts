import { mockProductLists } from '../../mocks/product.mock';
import { Product } from '../../types/product.type';
import useAxios from '../shared/useAxiosWrapper';

/**
 * Topic: Manage Products
 *
 * Feature: Get product detail
 *
 * @returns
 */
function useProductDetail(id: number) {
  return useAxios<Product>(
    {
      method: 'GET',
      url: '/products/' + id,
    },
    {
      mockData: mockProductLists.find(p => p.id == id),
    },
  );
}

export default useProductDetail;
