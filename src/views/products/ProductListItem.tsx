import React, { useState } from 'react';
import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Chip,
  Collapse,
  IconButton,
  Typography,
} from '@mui/material';
import { Product } from '../../types/product.type';
import { useRouter } from 'next/router';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import ProductEditDialog from './ProductEditDialog';
import ProductDeleteDialog from './ProductDeleteDialog';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { useCart } from '../../hooks/shared/useCart';

type Props = {
  product: Product;
  isNew?: boolean;
};

function ProductListItem({ product, isNew = false }: Props) {
  const router = useRouter();
  const { addItem } = useCart();
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [visible, setVisible] = useState(true);
  const [data, setData] = useState(product);

  const toggleEdit = () => {
    setOpenEdit(prev => !prev);
  };

  const toggleDelete = () => {
    setOpenDelete(prev => !prev);
  };

  const toggleVisible = () => {
    setVisible(prev => !prev);
  };

  const handleItemClick = () => {
    router.push(`/products/${product.id}`);
  };

  const handleUpdatedProduct = (newData: Product) => {
    setData(newData);
  };

  const handleAddToCart = () => {
    addItem({ ...product, quantity: 1 });
  };

  return (
    <Collapse in={visible}>
      <Card sx={{ minWidth: 300 }}>
        <CardActionArea onClick={handleItemClick} sx={{ backgroundColor: 'primary.main' }}>
          <CardMedia component={'img'} image={data.thumbnail} height='180' />
          {isNew && (
            <Chip label='Just Created' color='warning' size='small' sx={{ position: 'absolute', top: 12, right: 12 }} />
          )}
        </CardActionArea>
        <CardContent>
          <Typography gutterBottom variant='h5'>
            {data.name}
          </Typography>
          <Typography>${data.price.toFixed(2)}</Typography>
        </CardContent>
        <CardActions>
          <Button startIcon={<EditIcon />} onClick={toggleEdit}>
            Edit
          </Button>
          <Button color='error' startIcon={<DeleteIcon />} onClick={toggleDelete}>
            Delete
          </Button>
          <Box flexGrow={1} />
          <IconButton onClick={handleAddToCart}>
            <AddShoppingCartIcon />
          </IconButton>
        </CardActions>
        <ProductEditDialog open={openEdit} onClose={toggleEdit} initialData={data} onUpdated={handleUpdatedProduct} />
        <ProductDeleteDialog open={openDelete} onClose={toggleDelete} onDeleted={toggleVisible} data={data} />
      </Card>
    </Collapse>
  );
}

export default ProductListItem;
