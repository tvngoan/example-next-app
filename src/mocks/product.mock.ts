import { Product } from '../types/product.type';
import { faker } from '@faker-js/faker';

export const mockProductLists: Product[] = [];

for (let index = 0; index < 99; index++) {
  mockProductLists.push({
    id: faker.helpers.unique(faker.datatype.number),
    name: faker.commerce.productName(),
    thumbnail: faker.image.animals(500, 500, true),
    price: parseFloat(faker.commerce.price()),
  });
}
