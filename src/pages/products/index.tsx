import { Container } from '@mui/material';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Head from 'next/head';
import React, { useCallback, useState } from 'react';
import { Product, ProductGetParams } from '../../types/product.type';
import ProductFilter from '../../views/products/ProductFilter';
import ProductList from '../../views/products/ProductList';

function ProductPage() {
  const { t } = useTranslation();
  const [filter, setFilter] = useState<ProductGetParams>({});
  const [justCreatedProducts, setJustCreatedProducts] = useState<Product[]>([]);

  const handleChangeFilter = useCallback((newFilter: ProductGetParams) => {
    setFilter(newFilter);
  }, []);

  const handleCreatedProduct = useCallback((data: Product) => {
    setJustCreatedProducts(prev => [data, ...prev]);
  }, []);

  return (
    <>
      <Head>
        <title>{t('Products')}</title>
      </Head>
      <Container maxWidth='xl'>
        <ProductFilter filter={filter} onChange={handleChangeFilter} onCreatedProduct={handleCreatedProduct} />
        <ProductList filter={filter} justCreatedProducts={justCreatedProducts} />
      </Container>
    </>
  );
}

export const getServerSideProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});

export default ProductPage;
