import {
  Avatar,
  Badge,
  Box,
  Button,
  Fab,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Popover,
  Typography,
} from '@mui/material';
import React from 'react';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import CloseIcon from '@mui/icons-material/Close';
import { useCart } from '../../hooks/shared/useCart';

function FloatingCart() {
  const { state, removeItem } = useCart();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Fab color='primary' onClick={handleClick} sx={{ position: 'fixed', bottom: 32, right: 32 }}>
        <Badge color='error' badgeContent={state.items.length}>
          <ShoppingCartIcon />
        </Badge>
      </Fab>
      <Popover
        open={!!anchorEl}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
      >
        <List dense>
          {!state.items.length && (
            <ListItem>
              <ListItemAvatar>
                <Avatar variant='rounded'>?</Avatar>
              </ListItemAvatar>
              <ListItemText primary={'No item added yet'} secondary={0} />
            </ListItem>
          )}
          {state.items.map((item, index) => (
            <ListItem key={item.id}>
              <ListItemAvatar>
                <Avatar src={item.thumbnail} variant='rounded' />
              </ListItemAvatar>
              <ListItemText primary={item.name} secondary={`${item.quantity} x ${item.price.toFixed(2)}`} />
              <ListItemSecondaryAction>
                <IconButton onClick={() => removeItem(index)}>
                  <CloseIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
        <Box p={2} pt={0} display='flex' alignItems='center' justifyContent='space-between' minWidth={360}>
          <Typography variant='h6'>
            <strong>($) {state.totalAmount}</strong>
          </Typography>
          <Button>Checkout</Button>
        </Box>
      </Popover>
    </>
  );
}

export default FloatingCart;
