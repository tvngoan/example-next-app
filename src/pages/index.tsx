import { Button, Container } from '@mui/material';
import type { NextPage } from 'next';
import Head from 'next/head';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';

const Home: NextPage = () => {
  const { t } = useTranslation();
  const router = useRouter();

  const handleClick = () => {
    router.push('/products');
  };
  return (
    <>
      <Head>
        <title>{t('Home')}</title>
      </Head>
      <Container maxWidth='xl'>
        <Button onClick={handleClick}>{t('See Products')}</Button>
      </Container>
    </>
  );
};

export const getServerSideProps = async ({ locale }: { locale: string }) => ({
  props: {
    ...(await serverSideTranslations(locale, ['common'])),
  },
});

export default Home;
