import { Alert, CircularProgress, Grid } from '@mui/material';
import React from 'react';
import useProductList from '../../hooks/product/useProductList';
import { Product, ProductGetParams } from '../../types/product.type';
import ProductListItem from './ProductListItem';

type Props = {
  filter: ProductGetParams;
  justCreatedProducts: Product[];
};

function ProductList({ filter, justCreatedProducts }: Props) {
  const [{ data, error, loading }] = useProductList(filter);

  return (
    <Grid container spacing={2} justifyContent='stretch'>
      {loading && (
        <Grid item xs={12} sx={{ textAlign: 'center' }}>
          <CircularProgress />
        </Grid>
      )}

      {error && (
        <Grid item xs={12}>
          <Alert severity='error'>{error.message}</Alert>
        </Grid>
      )}

      {!loading && !data?.length && (
        <Grid item xs={12}>
          <Alert severity='warning'>{'No data found'}</Alert>
        </Grid>
      )}

      {justCreatedProducts.map(product => (
        <Grid item xs key={product.id}>
          <ProductListItem product={product} isNew />
        </Grid>
      ))}

      {data &&
        data.map(product => (
          <Grid item xs key={product.id}>
            <ProductListItem product={product} />
          </Grid>
        ))}
    </Grid>
  );
}

export default ProductList;
